let params = new URLSearchParams(window.location.search);
console.log(window.location.search);
console.log(params);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');
console.log(courseId);

let courseName = document.querySelector('#courseName');
let courseDescription = document.querySelector('#courseDescription');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

fetch(`http://localhost:4000/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		courseName.innerHTML = data.name;
		courseDescription.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
		enrollContainer.innerHTML = 
		`
		<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>
		`

		document.querySelector('#enrollButton').addEventListener('click', () => {
			fetch(`http://localhost:4000/api/users/enroll`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `bearer ${token}`
				},
				body: JSON.stringify({ courseId })
			})
			.then(res => res.json())
			.then(data => {
				if (data === true) {
					//redirect to course page
					alert('Thank you for enrolling');
					window.location.replace('./course.html');
				} else {
					// redirect to creating course
				}
			})
		});
	});