let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener('submit', (e) => {
	e.preventDefault();
	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;

	if (email === '' || password === '') {
		return alert('Please input both email and password');
	}

	fetch('http://localhost:4000/api/users/login', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email,
			password
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		if (data.accessToken) {
			localStorage.setItem('token', data.accessToken);

			fetch('http://localhost:4000/api/users/details', {
				method: 'GET',
				headers: {
					'Authorization': `bearer ${data.accessToken}`
				}
			})
			.then(res => res.json())
			.then(data => {

				localStorage.setItem('id', data._id);
				localStorage.setItem('isAdmin', data.isAdmin);
				window.location.replace('./courses.html');
			});
		} else {
			alert('Wrong credentials');
		}
	})
	.catch(err => {
		alert('Somethign went wrong, please try again')
		console.error(err)
	});
})