let adminUser = localStorage.getItem('isAdmin');

fetch('http://localhost:4000/api/courses')
	.then(res => res.json())
	.then(data => {
		console.log(data)

		let courseData;

		if (data.length < 1) {
			courseData = "No Courses currently available";
		} else {
			courseData = data.map(course => {
				console.log(course);
				let cardFooter;

				if (adminUser === "false" || !adminUser) {
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}"
							value=${course._id} class="btn-primary	btn text-white btn-block editButton">
							Select Course
						</a>
					`
				} else {
					cardFooter =
					`
					<a href="./editCourse.html?courseId=${course._id}"
						value=${course._id} class="btn-primary	btn text-white btn-block editButton">
						Edit
					</a>

					<a href="./deleteCourse.html?courseId=${course._id}"
						value=${course._id} class="btn-primary	btn text-white btn-block dangerButton">
						Delete
					</a>
					`
				}

				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>	
					</div>

					`
				)
			}).join("");
		}

		let container = document.querySelector('#coursesContainer');
		container.innerHTML = courseData;
	})

let modalButton = document.querySelector('#adminButton');

if (adminUser === "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse" class="btn btn-block btn-primary">Add Course</a>
	</div>
	`
}



